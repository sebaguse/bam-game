-*- Bust'A'Move -*-

	Program:	Sebastian Guz

	Graphics:	Mariusz Pyka

	Music:		Groveland Road Productions
			'Hands On' MIDI Software Ltd
			and others (no data available...)

1.	Launching the game.

This version of Bust'A'Move is using DOSBox emulator which allows to run the game in modern systems like Windows 7 x64 (and probably the other 64-bit systems). In Windows, run the game by clicking on the icon 'Bam' on Desktop or 'start.bat. " In DOS or command line, type "start.bat" (without the quotation marks!) And hit Enter.


2.	Control.

You can control the game by a mouse (recommended!) Or keyboard.

a) mouse:
indication of the menu - forward, backward;
select - left mouse button
switching option in the OPTIONS menu - left and right mouse button;
move the arrow - left, right;
launch balls - left mouse button.

b) keyboard:
indication of the menu - the up arrow, down arrow;
menu selection - space;
switching option in the OPTIONS menu - left arrow
right arrow;
launch balls - space.

Pause during the game - press 'P'.


3.	Options.

a) main menu

HOME GAME - starting the game;
OPTIONS - options (see below);
HIGH SCORES - the best results;
ABOUT - information about the authors;
TO QUIT dos - back to MS-DOS or Windows.

b) OPTIONS menu

CONTROLLER - the choice to operate the game controller (mouse or keyboard, in the absence of mouse or do not install the driver is only available keyboard);

CONTROLLER SENSITIVITY - controller sensitivity setting;

LEVEL - the level of difficulty, it affects the amount of the initial spheres, reducing the frequency of the board and the amount of time to launch the ball;

STEP BALL - affects the speed of the game, the smaller the value, the faster the ball moves fired (if you have fast computer - set the value of smaller, if slow, i.e.an old 486 set the value to the maximum, for the other value should be chosen experimentally);

BALL DESIGN - choose appearance of balls;
SAVE OPTIONS - save settings to disk;
RETURN TO MENU - Return the main Domain.