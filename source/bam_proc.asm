.MODEL MEDIUM,C

.CODE

EXTRN double_buffer:DWORD
EXTRN back:DWORD
EXTRN offs:WORD
EXTRN height:WORD

EXTRN b_wi:WORD
EXTRN b_he:WORD
EXTRN b_source:DWORD
EXTRN b_offsa:WORD
EXTRN b_offsb:WORD

PUBLIC BAMRemSprite32
PUBLIC BAMSaveSpriteBack32
PUBLIC FBlitBufferDW32
PUBLIC FClearDBuffer32

BAMRemSprite32 PROC FAR C
	.386
		  pusha
		  push ds
		  push es
	push ss
	les di,double_buffer
	lds si,back
	add di,offs
	mov ax,5
	mov bx,height

rs_loop01:
	mov cx,ax
	cld
	rep movsd
	add di,300
	dec bx
	jnz short rs_loop01
	pop ss
	pop es
	pop ds
	popa
	ret

BAMRemSprite32 ENDP

BAMSaveSpriteBack32 PROC FAR C
	.386
		  pusha
		  push ds
		  push es
	push ss
	les di,back
	lds si,double_buffer
	add si,offs
	mov ax,5
	mov bx,height

ssb_loop01:
	mov cx,ax
	cld
	rep movsd
	add si,300
	dec bx
	jnz short ssb_loop01
	pop ss
	pop es
	pop ds
	popa
	ret
BAMSaveSpriteBack32 ENDP

FBlitBufferDW32 PROC FAR C
	.386
	pusha
	push ds
	push es
	push ss
	lds si,b_source
	les di,double_buffer
	add si,b_offsa
	add di,b_offsb
	mov ax,b_wi
	mov bx,b_he
	mov dx,320
	sub dx,ax
	sub dx,ax

bb_loop01:
	mov cx,ax
	cld
	rep movsd
	add di,dx
	dec bx
	jnz short bb_loop01
	pop ss
	pop es
	pop ds
	popa
	ret
FBlitBufferDW32 ENDP

FClearDBuffer32 PROC FAR C
	.386
	pusha
	push ds
	push es
	les di,double_buffer
	xor eax,eax
	mov cx,16000
	rep stosd
	pop es
	pop ds
	popa
	ret
FClearDBuffer32 ENDP
END

